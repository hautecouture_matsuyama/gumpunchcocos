
var userdata = {
    highScore: 0,
};

cc.Class({
    extends: cc.Component,

    properties: {

    },

    writeScore: function(score){
        cc.sys.localStorage.setItem('UserData', JSON.stringify(score));
    },

    readScore: function(){
        var data = cc.sys.localStorage.getItem('UserData');

        if(data !== null){
            var getData = JSON.parse(data);
            return getData;
        }
        else{
            return null;
        }
    },
});
