var Gauge = require('Gauge');
var Score = require('Score');
var Audio = require('AudioManager');
var ObjectTower = require('ObjectTower');

var PlayerState = cc.Enum({
    RIGHT: "right",
    LEFT:  "left",
    DIE:   "die",
});

var PlayerSpriteType = cc.Enum({
    IDLE: "idle",
    ATTACK:  "attack",
    DIE: "die",
});

//グローバル変数にしておく（そのうちもっといい方法が見つかるかも）
var _state = PlayerState.LEFT;


cc.Class({
    extends: cc.Component,

    properties: {
        //通常状態の画像
        idleSprite:{
            default: null,
            type: cc.SpriteFrame,
        },
    　　//パンチ状態の画像
        attackSprite:{
            default: null,
            type: cc.SpriteFrame,
        },
        //ゲームオーバー状態の画像
        dieSprite:{
            default: null,
            type: cc.SpriteFrame,
        },

        //タップ表示のUI
        tapIcon:{
            default: null,
            type: cc.Node
        },

        objectower: {
            default: null,
            type: cc.Node
        },

        //ゲージ
        timeGauge: {
            default: null,
            type: Gauge
        },

        score:{
            default: null,
            type: Score,
        },

        audio: {
            default: null,
            type: Audio
        },

        tower: {
            default: null,
            type: ObjectTower
        },

        starEffect: {
            default: null,
            type: cc.ParticleSystem
        },
        
        smashPrefab:{
            default: null,
            type: cc.Prefab,
        },
    },

    statics:{
        PlayerSpriteType,
    },

    onLoad () {
        this.playerMove();
    },
    
    start (){
        this.initialize();
    },

    //初期化処理
    initialize: function(){
        var self = this;
        var screenWigth = cc.winSize.width;
        self.setPlayerSprite("idle");
        self.node.x = (screenWigth / 4) * -1;
        self.node.setScale(0.35, 0.35);
        _state = PlayerState.LEFT;
        cc.director.getCollisionManager().enabled = true;
    },

    onCollisionEnter: function(other, self){
        if(other.node.name == "hectorArm"){
            this.timeGauge.zeroBar();
        }
    },


    //PCでの操作周り（あとで直したい）
    InputForPc : function(player, screenWigth){
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyReleased: function(keyCode, event) {
                player.tapIcon.active = false;

                //ゲームオーバー時には操作できないようにする
                if(_state == PlayerSpriteType.DIE)
                    return;

                switch(keyCode){
                    case cc.KEY.left:
                        player.node.x = (screenWigth / 4) * -1;
                        player.node.setScale(0.35, 0.35);
                        //状態の変更
                        if(_state != PlayerState.LEFT){
                            _state = PlayerState.LEFT;
                        }
                        else{
                            player.playerAttack(_state);
                        }

                        break;

                    case cc.KEY.right:
                        player.node.x = screenWigth / 4;
                        player.node.setScale(-0.35, 0.35);
                        //状態の変更
                        if(_state != PlayerState.RIGHT){
                            _state = PlayerState.RIGHT;
                        }
                        else{
                            player.playerAttack(_state);
                        }
                        break;
                }

            },
        }, player.node);



    },

    //モバイルの操作
    InputForMobile : function(player, screenWigth){
        //タッチされたときの処理
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            onTouchBegan: function(touch, event) {
                player.tapIcon.active = false;
                    return true;
            },
            onTouchEnded: function(touch, event) {
                //ゲームオーバー時には操作できないようにする
                if(_state == PlayerSpriteType.DIE)
                    return;

                //画面の右
                if(touch.getLocation().x > screenWigth / 2)
                { 
                    player.node.x = screenWigth / 4;
                    player.node.setScale(-0.35, 0.35);
                    //状態の変更
                    if(_state != PlayerState.RIGHT){
                        _state = PlayerState.RIGHT;
                    }
                    else{
                        player.playerAttack(_state);
                    }
                }
                //画面の左
                else
                {
                    player.node.x = (screenWigth / 4) * -1;
                    player.node.setScale(0.35, 0.35);
                    //状態の変更
                    if(_state != PlayerState.LEFT){
                        _state = PlayerState.LEFT;
                    }
                    else{
                        player.playerAttack(_state);
                    }
                }
                
                return true;
            }
        }, player.node);

    },

    //Playerの動き
    playerMove : function(){
        var player =  this;
        var screenWigth = cc.winSize.width;

        //PCではキーボードでも可能
        player.InputForPc(player, screenWigth);
        
        //モバイルとPCのタッチ操作
        player.InputForMobile(player, screenWigth);
    },

    //攻撃時の処理
    playerAttack: function(direction){
        var self = this;
        self.setPlayerSprite("attack");
        self.timeGauge.recovery();
        self.score.addScore();
        self.audio.playSE("attack");
        self.tower.damageTower(direction);

        self.playParticle(self);

        setTimeout(() => {
            if(_state != PlayerState.DIE){
                this.setPlayerSprite("idle");
            }
        }, 250);
    },

    //死亡時の処理
    playerDie: function(){
        this.setPlayerSprite("die");
        _state = PlayerState.DIE;
        
    },

    //Playerの復活処理
    rivivalPlayer: function(){
        this.setPlayerSprite("idle");
        _state = PlayerState.IDLE;
    },

    //Player(ガムボール)のSpriteを変更する処理
    setPlayerSprite: function(sprite){
        var playerSprite = this.getComponent(cc.Sprite);
        switch(sprite){
            case PlayerSpriteType.IDLE:
                playerSprite.spriteFrame = this.idleSprite;
                break;
            case PlayerSpriteType.ATTACK:
                playerSprite.spriteFrame = this.attackSprite;
                break;
            case PlayerSpriteType.DIE:
                playerSprite.spriteFrame = this.dieSprite;
                break;
        }
    },

    //パンチ時のパーティクル生成（クソコードすぎワロタ）
    playParticle: function(self){
        var object = cc.instantiate(self.smashPrefab);
        var objectChild = object.getChildByName("star");
        self.objectower.addChild(object);
        objectChild.x = Math.floor( Math.random() * (25 + 1 - (-25)) ) + (-25) ;
        objectChild.y = Math.floor( Math.random() * (25 + 1 - (-25)) ) + (-25) ;
        objectChild.rotation = Math.floor( Math.random() * (180 + 1 - 1) ) + 1 ;

        setTimeout(() => {
            self.deleteObject(object);
        }, 500);
    },

    deleteObject: function(object){
        object.destroy();

    },

    getIsFirstTap : function(){
        var self = this;
        return self.tapIcon.enabled;
    },

});
