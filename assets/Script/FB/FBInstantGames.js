var bordName = 'test';

//広告のID
var unitID = '1506863982793367_1508192415993857';

var preloadedRewardVideo = null;

cc.Class({
    extends: cc.Component,

    properties:{
        content:{
            default: null,
            type: cc.Node
        },
        gameManager:{
            default: null,
            type: cc.Node
        },

        debugText:{
            default: null,
            type: cc.Label
        },
    },

    start(){
        this.debugText.string = "start";
        //this.InitializeAd();

        if(typeof FB == 'undefined'){
            console.log('見つからないよ');
            return;
        }
        console.log('みつかったよ！！！');

    },


    //スコア送信
    sendScore(score){
        if (typeof FBInstant === 'undefined') 
        return;

        FBInstant
        .getLeaderboardAsync(bordName)
        .then(leaderboard => {
            console.log(leaderboard.getName());
            return leaderboard.setScoreAsync(score, '{race: "elf", lavel:3');
        })
        .then(()=> console.log('save'))
        .catch(error => console.error(error));

        FBInstant.updateAsync({
            action: 'LEADERBOARD',
            name: bordName
          })
            .then(() => console.log('Update Posted'))
            .catch(error => console.error(error));
    },

    //自分（Player）のランキング情報を取得
    getMyRank(myRankNode){
        if (typeof FBInstant === 'undefined') 
        return;

        var playerIcon = myRankNode.getChildByName('player_icon').getComponent(cc.Sprite);
        var name = myRankNode.getChildByName('name_text').getComponent(cc.Label);
        var ranks = myRankNode.getChildByName('ranks');
        var rank = ranks.getChildByName('rank_text').getComponent(cc.Label);
        var score = myRankNode.getChildByName('score_text').getComponent(cc.Label);

        FBInstant
        .getLeaderboardAsync(bordName)
        .then(leaderboard => leaderboard.getPlayerEntryAsync())
        .then(entry => {
            var photoUrl = entry.getPlayer().getPhoto();
            cc.loader.load(photoUrl, (err, texture) => {
                playerIcon.spriteFrame = new cc.SpriteFrame(texture);
            });
            rank.string = this.rankNumber(entry.getRank());
            name.string = entry.getPlayer().getName();
            
            score.string = entry.getScore();
        }).catch(error => {
                var photoUrl = FBInstant.player.getPhoto();
                cc.loader.load(photoUrl, (err, texture) => {
                    playerIcon.spriteFrame = new cc.SpriteFrame(texture);
                });
                rank.string = '';
                name.string = FBInstant.player.getName();
                score.string = '0';
        });
   },

   //１位から指定した順位までを取得
   getRankingData(itemPrefab){
       var self = this;
       var photoUrl = new Array();
       var icon = new Array();

        FBInstant
        .getLeaderboardAsync(bordName)
        .then(leaderboard => leaderboard.getEntriesAsync(20, 0))
        .then(entries => {
        for (var i = 0; i < entries.length; i++) {
            var rankItem = cc.instantiate(itemPrefab);
            self.content.addChild(rankItem);
            var playerIcon = rankItem.getChildByName('player_icon').getComponent(cc.Sprite);
            icon[i] = playerIcon;
            var name = rankItem.getChildByName('name_text').getComponent(cc.Label);
            var ranks = rankItem.getChildByName('ranks');
            var rank = ranks.getChildByName('rank_text').getComponent(cc.Label);
            var score = rankItem.getChildByName('score_text').getComponent(cc.Label);

            photoUrl[i] = entries[i].getPlayer().getPhoto();

            rank.string = this.rankNumber(entries[i].getRank());
            name.string = entries[i].getPlayer().getName();            
            score.string = entries[i].getScore();

            cc.loader.load(photoUrl[i], (err, texture) => {
                var index = photoUrl.indexOf(texture.url);
                icon[index].spriteFrame = new cc.SpriteFrame(texture);
            });
        }
       
        }).catch(error => console.error(error));
   },


   rankNumber(rank){
       console.log('ランクは' + rank);
       var number = 'th';
       switch(rank){
           case 1:
                number = 'st';
                break;
           case 2:
                number = 'nd';
                break;
           case 3:
                number = 'rd';
                break;
       }

       return rank + number;


   },


    onShowRanking(){
        if (typeof FBInstant === 'undefined') {
            console.log('とじる');
            return;
        }

        var bordName = 'test';
        FBInstant
        .getLeaderboardAsync(bordName)
        .then(leaderboard => {
            console.log(leaderboard.getName());
            return leaderboard.setScoreAsync(1145141919, '{race: "elf", lavel:3');
        })
        .then(()=> console.log('save'))
        .catch(error => console.error(error));

        FBInstant.updateAsync({
            action: 'LEADERBOARD',
            name: 'test'
          })
            .then(() => console.log('Update Posted'))
            .catch(error => console.error(error));


    },
    
    onUpdateRanking (){
  
            FBInstant
  .getLeaderboardAsync('test')
  .then(leaderboard => leaderboard.getEntriesAsync(10, 0))
  .then(entries => {
    for (var i = 0; i < entries.length; i++) {
      console.log(
        entries[i].getRank() + '. ' +
        entries[i].getPlayer().getName() + ': ' +
        entries[i].getScore()
      );
    }
  }).catch(error => console.error(error));
    },

    // fbシェア機能
    onShareGame () {
        if (typeof FBInstant === 'undefined') return;
        FBInstant.shareAsync({
            intent: 'SHARE',
            image: this.getImgBase64(),
            text: 'おかしなガムボール 空手マスターへの道 をプレイしたよ',
            data: {myReplayData: '...'},
        }).then(() => {
            // continue with the game.
        });
    },

    // 截屏返回 iamge base6，用于 Share
    getImgBase64 () {
        let target = cc.find('Canvas');
        let width = cc.winSize.width, height = cc.winSize.height;
        let renderTexture = new cc.RenderTexture(width, height);
        renderTexture.begin();
        target._sgNode.visit();
        renderTexture.end();
        //
        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext('2d');
        canvas.width = width;
        canvas.height = height;
        if (cc._renderType === cc.game.RENDER_TYPE_CANVAS) {
            let texture = renderTexture.getSprite().getTexture();
            let image = texture.getHtmlElementObj();
            ctx.drawImage(image, 0, 0);
        }
        else if (cc._renderType === cc.game.RENDER_TYPE_WEBGL) {
            let buffer = gl.createFramebuffer();
            gl.bindFramebuffer(gl.FRAMEBUFFER, buffer);
            let texture = renderTexture.getSprite().getTexture()._glID;
            gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);
            let data = new Uint8Array(width * height * 4);
            gl.readPixels(0, 0, width, height, gl.RGBA, gl.UNSIGNED_BYTE, data);
            gl.bindFramebuffer(gl.FRAMEBUFFER, null);
            let rowBytes = width * 4;
            for (let row = 0; row < height; row++) {
                let srow = height - 1 - row;
                let data2 = new Uint8ClampedArray(data.buffer, srow * width * 4, rowBytes);
                let imageData = new ImageData(data2, width, 1);
                ctx.putImageData(imageData, 0, row);
            }
        }
        return canvas.toDataURL('image/png');
    },

    //広告の初期化
    InitializeAd() { 
        var self = this;

        self.debugText.string = "Init";

        if (typeof FBInstant === 'undefined') 
        return;
        
        FBInstant.getRewardedVideoAsync(
            self.unitID).then(function(rewarded){
                self.preloadedRewardVideo = rewarded;
                return self.preloadedRewardVideo.loadAsynfc();
            }).then(function(){
                self.debugText.string = "成功";

            }).catch(function(err){
                self.debugText.string = "エラー" + err.message;                
            });
    },
    //広告の表示
    showReward() {
        var self = this;
        self.debugText.string = "広告読み込み";

        self.preloadedRewardedVideo.showAsync()
        .then(function() {
        // Perform post-ad success operation
        console.log('Rewarded video watched successfully');
        self.gameManager.getComponent("GameManager").retryGame(false);
        self.debugText.string = "広告読み込み成功";


        })
        .catch(function(e) {
            self.gameManager.getComponent("GameManager").retryGame(true);

        });
    },
});
