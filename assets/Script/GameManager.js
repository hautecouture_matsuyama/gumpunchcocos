var Gauge = require('Gauge');
var Audio = require('AudioManager');
var Score = require('Score');
var ObjectTower = require('ObjectTower');

var FBInstantGame = require('FBInstantGames');

var isReward = false;

var State = cc.Enum({
    TITLE: "title",
    GAME:  "game",
    RESULT: "result",
});

cc.Class({
    extends: cc.Component,

    properties:{
        state: State.TITLE,
        //残り時間のゲージ
        timeGauge: {
            default: null,
            type: Gauge
        },
        //AudioManager
        audio: {
            default: null,
            type: Audio
        },
        //Score管理
        score:{
            default: null,
            type: Score
        },

        fbinstantGame:{
            default: null,
            type: FBInstantGame,
        },

        objectoTower:{
            default: null,
            type: ObjectTower,
        },

        uiManager:{
            default: null,
            type: cc.Node
        },

        player:{
            default: null,
            type: cc.Node
        },
        tower:{
            default: null,
            type: cc.Node
        },
        //タイトルのUI群
        resultScreen:{
            default: null,
            type: cc.Node
        },

        tapIcon:{
            default: null,
            type: cc.Node
        }
    },

    onLoad: function(){
        this.state = State.TITLE;
    },

    update: function(dt){
        switch(this.state){
            case State.TITLE:
                break;
            case State.GAME:
                this.game(dt);
                break;
            case State.RESULT:
                break;
        }
    },


    //ゲームの開始
    gameStart: function(){
        console.log("GameStart");
        this.state = State.GAME;
        this.enableNode(this.player);
        this.enableNode(this.tower);
        this.audio.playBGM();
    },

    //ゲーム中の処理
    game: function(dt){
        if(this.tapIcon.active == false){
            if(this.timeGauge.progressBar(dt) <= 0){
                this.gameOver();
            }
        }
    },

    //ゲームオーバー
    gameOver: function(){
        this.state = State.RESULT;
        var playerComponent = this.player.getComponent("Player");
        playerComponent.playerDie();
        this.audio.stopBGM();
        this.audio.playSE("die");

        //this.fbinstantGame.InitializeAd();

        //リザルト画面の表示は少し遅らせる
        setTimeout(() => {

            this.enableNode(this.resultScreen);
            this.score.setResultScore();
            //動画リワードを表示するかの判定
            
/*
            if(isReward == false ){
                isReward = true;
                var self = this; 
                self.uiManager.getComponent("UIManager").showRewardScreen();
            }
            else{
                isReward = false;
            }        */
        
        
        }, 700);

            

    },

    //ゲームのリトライ
    //リトライに必要な処理はだいたいここでやっているはず
    retryGame: function(isInitialize){
        var self = this;
        self.state = State.GAME;
        self.audio.playBGM();
        self.objectoTower.initialize();
        self.timeGauge.initializeGauge();

        //スコアを初期化するかどうか（）
        self.score.initializeScore(isInitialize);
        
        var playerComponent = this.player.getComponent("Player");
        playerComponent.initialize();
        self.tapIcon.active = true;
    },

    //リワード広告を見ての復活
    revivalGame: function(){
        var self = this;
        console.log("REWARD!!!");
        self.retryGame(false);
    },


    //nodeの表示
    enableNode: function(node){
        node.active = true;
    },

    //nodeの非表示
    disableNode: function(node){
        node.active = false;
    },

});
