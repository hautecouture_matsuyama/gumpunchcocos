
cc.Class({
    extends: cc.Component,

    properties: {
        //ゴミの画像
        gomiObjectSprite:{
            default: [],
            type: cc.SpriteFrame
        },

        gomiObject:{
            default: [],
            type: cc.Node
        },
        gomiPrefab:{
            default: null,
            type: cc.Prefab
        },
    },

    onLoad () {
        this.initialize();
    },

    //初期の画像設定
    initialize: function(){
        var self = this;
        for(var i = 0; i < self.gomiObject.length; i++){
            var index = Math.floor(Math.random() * self.gomiObjectSprite.length)
            var sprite = self.gomiObject[i].getComponent(cc.Sprite);
            sprite.spriteFrame = self.gomiObjectSprite[index];
            self.gomiObject[i].y = 90 + (80*i);
        }
    },
    
    //ゴミのタワーの動き
    damageTower: function(direction){
        var self = this;
        for(var i = 0; i < self.gomiObject.length; i++){
            if(self.gomiObject[i].y - 80 > 80){
                self.gomiObject[i].y -= 80;
            }
            else{
                var sprite = self.gomiObject[i].getComponent(cc.Sprite);
                self.smashObject(direction, sprite.spriteFrame._name);
                self.gomiObject[i].y = 2410;//わかりにくすぎワロタ
                var index = Math.floor(Math.random() * self.gomiObjectSprite.length)
                sprite.spriteFrame = self.gomiObjectSprite[index];
            }
        }
    },

    //殴られたゴミが飛んでいく処理
    smashObject: function(direction, spriteName){
        var self = this;
        var dir = direction == "right" ? -1 : 1;

        var forceMin = 50 ;
        var forceMax = 150 ;
        var forceY = Math.floor( Math.random() * (forceMax + 1 - forceMin) ) + forceMin ;

        var rotateMin = 45 ;
        var rotateMax = 180 ;
        var rotateX = Math.floor( Math.random() * (rotateMax + 1 - rotateMin) ) + rotateMin ;

        var spawn = cc.spawn(
            cc.rotateBy(0.4, rotateX * dir),
            cc.jumpBy(0.5, 400 * dir, forceY, 10, 1)
        );
        var object = cc.instantiate(self.gomiPrefab);
        var sprite = object.getComponent(cc.Sprite);
        var index = self.getGomiSprite(spriteName);

        sprite.spriteFrame = self.gomiObjectSprite[index];
        self.node.addChild(object);
        object.runAction(spawn);

        setTimeout(() => {
            this.deleteObject(object);
        }, 1000);
    },

    deleteObject: function(object){
        object.destroy();

    },

    getGomiSprite: function(spriteName){
        var index = 1;
        switch(spriteName){
            case "gomi_blue":
                index = 0;
                break;
            case "gomi_red":
                index = 1;
                break;
            case "gomi_yellow":
                index = 2;
                break;
            case "gomi_orange":
                index = 3;
                break;
        }

        return index;
    },

});
