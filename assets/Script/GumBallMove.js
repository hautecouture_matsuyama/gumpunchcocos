var ActionType = cc.Enum({
    ATTACK: 0,
    JUMP: 1,
    MOVE: 2,
    TURN: 3,
});

cc.Class({
    extends: cc.Component,

    properties: {
        moveTime: 0.1,
        jumpForceX: 50,
        jumpHeight: 20,
        minActionInterval:500,
        maxActionInterval:1000,
        direction: 1,
        punchSprites:{
            default:[],
            type: cc.SpriteFrame
        },
        isMove: true,
    },

    statics:{
        ActionType
    },


    onEnable (){
        this.isMove = true;
        cc.director.getCollisionManager().enabled = true;
    },

    onCollisionEnter: function(other, self){
        switch(other.node.name){
            case "right_wall":
                this.direction = -1;
                break;
            case "left_wall":
                this.direction = 1;
                break;
        }
        this.moveMotion();
        this.node.setScale(0.35 * this.direction, 0.35);
    },

    start () {
        this.gumballController();
    },

    gumballController: function(){
        var actionInterval = Math.floor(Math.random() * (this.maxActionInterval + 1 - this.minActionInterval)) + this.minActionInterval;
        var action = Math.floor( Math.random() *  4) ;

    if(this.isMove){
        var sprite = this.getComponent(cc.Sprite);
        sprite.spriteFrame = this.punchSprites[0];

        switch(action){
            case ActionType.ATTACK:
                this.attackMotion();
                break;
            case ActionType.JUMP:
                this.jumpMotion();
                break;
            case ActionType.MOVE:
                this.moveMotion();
                break;
            case ActionType.TURN:
                this.turnMotion();
                break;
        }
    }

        setTimeout(() => {
            this.gumballController();
        }, actionInterval);
    },

    //攻撃
    attackMotion: function(){
        this.isMove = false;
        var punchCount = Math.floor(Math.random() * 4) + 1;
        while(punchCount > 0){
            for(var i = 0; i < 12; i++){
                var sprite = this.getComponent(cc.Sprite);
                sprite.spriteFrame =  this.punchSprites[7];
            }
            punchCount--;
        }
        this.isMove = true;
    },

    //ジャンプ
    jumpMotion: function(){
        var moveAction = cc.jumpBy(0.2, 0, 0, this.jumpHeight, 1);
        this.node.runAction(moveAction);
    },

    //移動
    moveMotion: function(){
        this.node.setScale(0.35 * this.direction, 0.35);
        var moveAction = cc.jumpBy(0.2, this.jumpForceX * this.direction, 0, 0.1, 1);
        this.node.runAction(moveAction);
    },

    //向きの変更
    turnMotion: function(){
        this.direction *= -1;
        this.node.setScale(0.35 * this.direction, 0.35);
    },
});
