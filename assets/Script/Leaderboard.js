var FBInstantGames = require('FBInstantGames');


cc.Class({
    extends: cc.Component,

    properties: {
        fBInstantGames: {
            default: null,
            type: FBInstantGames
        },

        myRankNode:{
            default: null,
            type: cc.Node
        },

        content:{
            default: null,
            type: cc.Node
        },

        itemPrefab:{
            default: null,
            type: cc.Prefab
        },
    },

    onEnable(){
        var self = this;
        self.fBInstantGames.getMyRank(self.myRankNode);
        self.fBInstantGames.getRankingData(self.itemPrefab);
    },

    onDisable(){
        var self = this;
        self.content.destroyAllChildren();
    },
});
