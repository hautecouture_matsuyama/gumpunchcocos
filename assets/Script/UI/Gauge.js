var Gauge = require('Gauge');

cc.Class({
    extends: cc.Component,

    properties: {
        //GameManager
        gameManager:{
            default: null,
            type: cc.Node
        },

        bar: {
            default: null,
            type: cc.ProgressBar
        },

        detectionValue:10,
        speedUpValue:5,
        recoveryValue:5

    },

    //ゲージ関連の値の初期化
    initializeGauge: function(){
        var self = this;
        self.detectionValue = 0.1;
        self.bar.progress = 1;

    },

    progressBar: function(dt){
        var self = this;
        if(self.bar.progress >= 0){
            self.bar.progress -= self.detectionValue*dt;
        } 
        return self.bar.progress;
    },

    //ゲームオーバー時に呼び出す
    zeroBar: function(){
        var self = this;
        self.bar.progress = 0;
    },

    //回復
    recovery: function(){
        var self = this;
        if(self.bar.progress < 1)
        self.bar.progress += self.recoveryValue;
    },

    //スピードアップ
    speedUp: function(){
        var self = this;
        self.detectionValue += self.speedUpValue;
    },



});
