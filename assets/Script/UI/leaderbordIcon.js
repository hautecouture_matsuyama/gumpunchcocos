var FBInstantGames = require('FBInstantGames');

cc.Class({
    extends: cc.Component,

    properties: {
        fBInstantGames:{
            default: null,
            type: FBInstantGames
        },

    },

    onLoad(){
        var sprite = this.getComponent(cc.Sprite);
        var photoUrl = fBInstantGames.photoUrl;
        cc.loader.load(photoUrl, (err, texture) => {
            sprite.spriteFrame = new cc.SpriteFrame(texture);
        });
    },

});
