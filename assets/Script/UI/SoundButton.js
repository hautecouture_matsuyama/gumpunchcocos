
cc.Class({
    extends: cc.Component,

    properties: {

        //サウンドON時のスプライト
        soundOn:{
            default: null,
            type: cc.SpriteFrame
        },
        //サウンドOFF時のスプライト
        soundOff:{
            default: null,
            type: cc.SpriteFrame
        },

    },

    onLoad(){
        var sprite = this.getComponent(cc.Sprite);
        if(cc.audioEngine.getMusicVolume() == 0){
            sprite.spriteFrame = this.soundOff;
        }
        else{
            sprite.spriteFrame = this.soundOn;
        }

    },

    clickEvent: function(){
        var sprite = this.getComponent(cc.Sprite);

        if(cc.audioEngine.getMusicVolume() > 0){
            cc.audioEngine.setMusicVolume(0);
            sprite.spriteFrame = this.soundOff;
        }
        else{
            cc.audioEngine.setMusicVolume(1);
            sprite.spriteFrame = this.soundOn;
        }
    },

});
