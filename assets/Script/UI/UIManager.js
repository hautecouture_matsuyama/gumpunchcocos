
var GameManager = require('GameManager');
var AudioManager = require('AudioManager');
var Player = require('Player');

cc.Class({
    extends: cc.Component,

    properties: {

        //GameManager
        gameManager:{
            default: null,
            type: GameManager
        },

        //遊び方
        howtoScreen:{
            default: null,
            type: cc.Node
        },

        //タイトルのUI群
        titleScreen:{
            default: null,
            type: cc.Node
        },

        //ゲームプレイ中のUI群
        gameScreen:{
            default: null,
            type: cc.Node
        },

        //リザルトのUI群
        resultScrenn:{
            default: null,
            type: cc.Node
        },

        //動画を見るのUI群
        rewardScreen:{
            default: null,
            type: cc.Node
        },

        //ランキング
        leaderbord:{
            default: null,
            type: cc.Node
        },
        
        fbInstantGame:{
            default: null,
            type: cc.Node
        },



    },

    //遊び方の表示
    showHowTo: function(){
        this.howtoScreen.active = true;
    },

    //サウンドON・OFF切替
    soundSwitch: function(){

    },

    //ゲームを開始する
    playGame: function(){
        this.gameManager.disableNode(this.titleScreen);
        this.gameScreen.active = true;
        this.gameManager.gameStart();
    },

    //ゲームのリトライ
    retryGame: function(){
        //cc.director.loadScene("main");
        this.gameManager.retryGame(true);
        this.resultScrenn.active = false;
    },

    //リワード画面の表示
    showRewardScreen: function(){
        var self = this;
        self.rewardScreen.active = true;
    },

    //動画リワード広告をみる
    onReward: function(){
        var self = this;
        self.fbInstantGame.getComponent("FBInstantGames").showReward();
    },


    openRanking(){
        this.leaderbord.active = true;
    }

});
