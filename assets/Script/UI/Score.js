var Gauge = require('Gauge');
var UserDataManager = require('UserDataManager');
var FBInstantGames = require('FBInstantGames');

var value = 0;
var maxSpeedLevel = 17;
var nowLevel = 0;

cc.Class({
    extends: cc.Component,

    properties: {
        //ゲーム中のスコアテキスト
        scoreText:{
            default: null,
            type: cc.Label
        },
        //レベルのテキスト
        levelText:{
            default: null,
            type: cc.Label
        },
        //現在のスコア
        nowScoreText:{
            default: null,
            type: cc.Label
        },
        //ベストスコア
        bestScoreText:{
            default: null,
            type: cc.Label
        },
        //残り時間のゲージ
        timeGauge: {
            default: null,
            type: Gauge
        },

        userDataManager: {
            default: null,
            type: UserDataManager
        },

        fBInstantGames: {
            default: null,
            type: FBInstantGames
        },

        backCharacter:{
            default:[],
            type: cc.Node
        },
    },

    onLoad(){
        value = 0;
        nowLevel = 0;
    },

    //スコアの初期化
    initializeScore: function(isInitialize){
        var self = this;
        if(isInitialize == true){
            console.log('スコアの初期化');
            value = 0;
            nowLevel = 0;
            self.scoreText.string = 0;
            //背景のキャラクターたちを非表示に
            for(var i = 0; i < self.backCharacter.length; i++){
                self.backCharacter[i].active = false;
            }
        }
        else{
            nowLevel = 0;            
        }
      
    },

    //スコア加算
    addScore: function(){
        var self = this;
        value++;
        self.scoreText.string = value;

        if(value % 20 == 0 && nowLevel < maxSpeedLevel){
            nowLevel++;
            
            var level = parseInt(value / 20);
            self.levelText.string = "レベル" + (level + 1);
            self.timeGauge.speedUp();

            setTimeout(() => {
                this.setLevelText();
            }, 1000);
        }
        if(value % 100 == 0){
            var idx = (value/100) - 1;
            self.backCharacter[idx].active = true;
        }
    },

    setLevelText: function(){
        this.levelText.string = "";
    },

    //ゲームオーバー時の最終スコアをセットする
    setResultScore: function(){
        var self = this;
        self.nowScoreText.string = value;
        self.fBInstantGames.sendScore(value);

        if(self.userDataManager.readScore() != null){
            //ベストスコアの更新
            if(self.userDataManager.readScore() <= value){
                self.userDataManager.writeScore(value);
                self.bestScoreText.string = value;
            }
            else{
                self.bestScoreText.string = self.userDataManager.readScore();
            }
        }
        else{
            self.userDataManager.writeScore(value);
            self.bestScoreText.string = value;
        
        }
    
    },

});
