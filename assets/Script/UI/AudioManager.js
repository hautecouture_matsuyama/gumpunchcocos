var SEtype = cc.Enum({
    ATTACK:  "attack",
    DIE: "die",
});

var isSound = true;

cc.Class({
    extends: cc.Component,

    properties: {
        //ゲーム中のBGM
        bgm:{
            default: null,
            url: cc.AudioClip
        },
        //BGMのID
        _bgmId: -1,

        //パンチ時のSE
        attackSe:{
            default: null,
            url: cc.AudioClip
        },

        //ゲームオーバー時のSE
        dieSe:{
            default: null,
            url: cc.AudioClip
        },
    },

    onLoad(){
        isSound = true;
    },

    //BGMの再生
    playBGM: function(){
        if(cc.audioEngine.getMusicVolume() > 0){
            var self = this;
            self._bgmId = cc.audioEngine.play(self.bgm, true);
        }
    },
    
    //BGMの停止
    stopBGM: function(){
        var self = this;
        cc.audioEngine.stop(self._bgmId);
        self._bgmId = -1;
    },

    //SEの再生
    playSE: function(name){
        if(cc.audioEngine.getMusicVolume() > 0){
            var self = this;
            switch(name){
                case SEtype.ATTACK:
                    cc.audioEngine.play(self.attackSe);
                    break;
                case SEtype.DIE:
                    cc.audioEngine.play(self.dieSe);
                    break;
            }
        }
    }


});
